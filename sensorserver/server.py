
import os, os.path
import random
import string
#import read
import cherrypy
from read_sensor import SensorReader

class SensorValues(object):
    @cherrypy.expose
    def index(self):
        return open('sensorWebsite.html')


@cherrypy.expose
class SensorValuesWebService(object):

    def GET(self):
	SensorReader.read_values()
	values = open("sensors.json").read()
	return values

if __name__ == '__main__':
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
        '/sensorvalues': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './public'
        }
    }
    webapp = SensorValues()
    webapp.sensorvalues = SensorValuesWebService()
    cherrypy.quickstart(webapp, '/', conf)
