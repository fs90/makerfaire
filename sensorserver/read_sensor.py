import os;
import time;
import serial;
import binascii;

class SensorReader:	
	def read_line_from_device(filename, default):
		device = os.open(filename, os.O_RDWR);
		line_array = os.read(device,64);
		line_array = line_array.splitlines();
		os.close(device);
		if len(line_array) > 1:
			values = line_array[0].strip();
			return values.decode('utf-8');
		return default;


	def read_finestaub_data(filename):
		ser = serial.Serial(filename, 9600);

		values = ser.read(10);

		value1 = int.from_bytes(values[2:4], byteorder='little', signed=False) / 10.0;
		value2 = int.from_bytes(values[4:6], byteorder='little', signed=False) / 10.0;

		ser.close();
		return(value1,value2)

	@staticmethod
	def read_values():
		airsensor_values = read_line_from_device("/dev/airsensorfs", "500");
		#movement_values = read_line_from_device("/dev/movementsensorfs", "0 10");
		#movement_values_fields = movement_values.split(" ");

		feinstaub_values = read_finestaub_data('/dev/ttyUSB0');

		output_file = open("sensors.json", "w");
		output_file.write("{\"air\": \"%s\", \"dust1\": \"%s\", \"dust2\": \"%s\"}\n" % (airsensor_values, feinstaub_values[0], feinstaub_values[1]));
		#output_file.write("0 AIRSENSOR voc=%s - VOC %s\n" % (airsensor_values, airsensor_values)); 
		#output_file.write("0 MOVEMENTSENSOR last=%s|duration=%s - last = %s, duration = %s\n" % (movement_values_fields[0], movement_values_fields[1], movement_values_fields[0], movement_values_fields[1])); 
		#output_file.write("0 FINEDUSTSENSOR ppm25=%s|ppm10=%s - ppm2.5 = %s, ppm10 = %s\n" % (feinstaub_values[0], feinstaub_values[1], feinstaub_values[0], feinstaub_values[1])); 
		output_file.close();
		#print(feinstaub_values);

