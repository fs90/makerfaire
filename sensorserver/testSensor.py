import json
import time

def loadValues():
	with open("sensorValues.json") as json_file:
		return json.load(json_file)

def storeValues(values):
	with open("sensorValues.json","w") as json_file:
		json.dump(values, json_file)

def main():
	while(1):
		sensorValues = loadValues()
		print "Value:" + sensorValues['temp']
		val = int(sensorValues['temp']) + 1
		sensorValues['temp'] = str(val)  
		storeValues(sensorValues)		
		time.sleep(2)

if __name__ == '__main__': 
	main()
