while [ true ]; do
   python3 /home/pi/read_sensor.py
   scp -p -i /home/pi/.ssh/id_rsa /home/pi/outputs/*.txt pi@192.168.91.254:/var/lib/check_mk_agent/spool
   sleep 5
   rm /home/pi/outputs/*
done
